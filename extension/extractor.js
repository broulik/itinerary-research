/*
    Copyright (C) 2019 Kai Uwe Broulik <kde@privat.broulik.de>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of
    the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

(function() {
    let result = {
        matches: []
    };

    console.log("%cSearching for interesting Itinerary metadata", "font-weight: bold");

    let schemaOrgTags = document.querySelectorAll("[itemtype*='schema.org']");
    if (schemaOrgTags.length > 0) {
        console.log("  Schema.org tags:", schemaOrgTags);
        result.matches.push("schemaOrgTags");

        let types = [];

        // Now extract the different types that it has
        schemaOrgTags.forEach((tag) => {
            let type = tag.getAttribute("itemtype");
            // Using https for an URI is wrong but some websites do that...
            type = type.replace(/^https?:\/\/s?schema.org\//, "");

            if (!types.includes(type)) {
                types.push(type);
            }
        });

        console.log("    Found", types);

        result.schemaOrgTypes = types;
    } else {
        console.log("  Schema.org tags:%c None", "color: red");
    }

    let jsonLdScriptTags = document.querySelectorAll("script[type='application/ld+json']");
    if (jsonLdScriptTags.length > 0) {
        console.log("  ld+json tags:", jsonLdScriptTags);
        result.matches.push("jsonLdScriptTags");

        let jsonLds = [];

        jsonLdScriptTags.forEach((tag) => {
            jsonLds.push(tag.innerText);
        });

        result.jsonLdScriptTags = jsonLds;
    } else {
        console.log("  ld+json tags: %cNone", "color: red");
    }

    let openGraphTypeTags = document.querySelectorAll("meta[property='og:type']");
    if (openGraphTypeTags.length > 0) {
        console.log("  Open Graph tags:", openGraphTypeTags);
        result.matches.push("opengraph");

        let openGraphTypes = [];

        openGraphTypeTags.forEach((tags) => {
            openGraphTypes.push(tags.content);
        });

        result.openGraphTypes = openGraphTypes;
    } else {
        console.log("  Open Graph type tags: %cNone", "color: red");
    }

    let boardingPasses = [];

    const checkBoarding = (tag) => {
        let children = tag.children || [];
        for (let i = 0; i < children.length; ++i) {
            checkBoarding(children[i]);
        }

        // Ignore SVG paths which likely have Move commands
        if (tag.tagName.toLowerCase() === "path") {
            return;
        }

        let attributes = tag.attributes || [];
        for (let i = 0; i < attributes.length; ++i) {
            let attribute = attributes[i];
            let value = attribute.value;

            if (value.length < 30) {
                continue;
            }

            // Boarding pass IATA BCBP cannot contain comma
            if (value.includes(",")) {
                continue;
            }

            // Ignore some tags that can clearly not be what we're looking for
            if (tag.tagName === "META") {
                if (tag.name === "google-site-verification") {
                    continue;
                }
            }

            if (!value.includes("M1") && !value.includes("M2")) {
                continue;
            }

            console.log("  Found potential boarding pass tag", tag);

            boardingPasses.push(value);
        }
    };
    console.time("Checking for boarding pass contents...");
    checkBoarding(document.documentElement);
    console.timeEnd("Checking for boarding pass contents...");

    if (boardingPasses.length > 0) {
        result.matches.push("boardingPasses");
        result.boardingPasses = boardingPasses;
    } else {
        console.log("  Boarding pass contents: %cNone", "color: red");
    }

    return result;
})();
