/*
    Copyright (C) 2019 Kai Uwe Broulik <kde@privat.broulik.de>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of
    the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var currentUrl = "";
var currentInfo = null;

function checkForMetadata() {
    currentUrl = "";
    currentInfo = null;
    showBadge(false);

    chrome.tabs.query({
        currentWindow: true,
        active: true
    }, (tabs) => {
        let tab = tabs[0];
        if (!tab) {
            return;
        }

        chrome.tabs.executeScript(tab.id, {
            file: "extractor.js",
            //allFrames: true,
        }, (result) => {
            if (chrome.runtime.lastError) {
                return;
            }

            // Returns results for each tab but we only queried forone
            result = result[0];

            currentUrl = tab.url;
            currentInfo = result;

            showBadge(result.matches.length > 0);
        });
    });
}

function showBadge(show) {
    if (show) {
        chrome.browserAction.setBadgeText({ text: "!" });
        chrome.browserAction.setBadgeBackgroundColor({ color: "#f67400" }); // breeze "neutral" color
    } else {
        chrome.browserAction.setBadgeText({ text: "" });
    }
}

chrome.tabs.onActivated.addListener(checkForMetadata);
chrome.windows.onFocusChanged.addListener(checkForMetadata);

chrome.tabs.onUpdated.addListener((tabId, info, tab) => {
    if (info.status === "complete") {
        checkForMetadata();
    }
});

if (typeof browser !== "undefined") {
    browser.runtime.onMessage.addListener((message) => {
        if (message.action === "getMatches") {
            return new Promise((resolve) => {
                resolve({ url: currentUrl, info: currentInfo });
            });
        }
    });
} else if (typeof chrome !== "undefined") {
    chrome.extension.onMessage.addListener((message, sender, sendResponse) => {
        if (message.action === "getMatches") {
            sendResponse({
                url: currentUrl,
                info: currentInfo
            });
        }
    });
}
