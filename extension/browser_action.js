/*
    Copyright (C) 2019 Kai Uwe Broulik <kde@privat.broulik.de>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of
    the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const handleMatches = (result) => {

    if (!result) {
        return;
    }

    document.getElementById("website-url").innerText = result.url || "UNKNOWN";

    (result.info.matches).forEach((info) => {
        let listItem = document.getElementById("available-metadata-" + info);
        if (listItem) {
            listItem.classList.add("checked");
        }
    });

    let schemaOrgTypesList = document.getElementById("schema-org-types-list");

    (result.info.schemaOrgTypes || []).forEach((item) => {
        let bullet = document.createElement("li");
        bullet.innerText = item;
        schemaOrgTypesList.appendChild(bullet);
    });

    let jsonLdScriptTagsList = document.getElementById("available-metadata-jsonLdScriptTags");

    (result.info.jsonLdScriptTags || []).forEach((item) => {
        let textArea = document.createElement("textarea");
        textArea.readOnly = true;
        textArea.rows = 10;
        try {
            textArea.innerHTML = JSON.stringify(JSON.parse(item), null, 2);
        } catch (e) {
            textArea.innerHTML = "Parse error!\n" + e;
            console.warn("Failed to parse jsonld", e);
        }
        jsonLdScriptTagsList.append(textArea);
    });

    let openGraphTypesList = document.getElementById("opengraph-types-list");

    (result.info.openGraphTypes || []).forEach((item) => {
        let bullet = document.createElement("li");
        bullet.innerText = item;
        openGraphTypesList.appendChild(bullet);
    });

    console.log("result", result);

};

document.addEventListener("DOMContentLoaded", () => {

    let msg = {action: "getMatches"};

    if (typeof browser !== "undefined" && browser.runtime.sendMessage) {
        browser.runtime.sendMessage(msg).then(handleMatches);
    } else if (typeof chrome !== "undefined" && chrome.runtime.sendMessage) {
        chrome.runtime.sendMessage(msg, handleMatches);
    }

});
